## Assumptions Made
1. There can have multiple words in a sentence with the longest/shorted length and 
it is expected to find all those words. 
2. Non-alphanumeric characters are removed at pre-processing step
3. Throw an illegalArgumentException when passing null to methods

## How to build and execute test
use *gradle test* command to run unit tests in the command line.

or import the project in an IDE, build the project and run all the tests in it.