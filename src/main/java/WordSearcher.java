import static java.util.Objects.isNull;

import java.util.ArrayList;
import java.util.List;

public class WordSearcher {
    
    private static final String ARGUMENT_SHOULD_NOT_BE_NULL = "Argument should not be null";
    
    public SearchResult getLongestWordsInSentence(String sentence) {
    if (isNull(sentence)) throw new IllegalArgumentException(ARGUMENT_SHOULD_NOT_BE_NULL);
    List<String> longestWords = new ArrayList<>();
    int maxLength = Integer.MIN_VALUE;
    String[] words = sentence.split(" ");
    for (String word : words) {
      word = word.replaceAll("[^\\w]", "");
      if (word.length() > maxLength) {
        longestWords.clear();
        maxLength = word.length();
        longestWords.add(word);
      } else if (word.length() == maxLength) {
        longestWords.add(word);
      }
    }

    return new SearchResult(longestWords, maxLength);
  }

  public SearchResult getShortestWordsInSentence(String sentence) {
    if (isNull(sentence)) throw new IllegalArgumentException(ARGUMENT_SHOULD_NOT_BE_NULL);
    List<String> shortestWords = new ArrayList<>();
    int minLength = Integer.MAX_VALUE;
    String[] words = sentence.split(" ");
    for (String word : words) {
      word = word.replaceAll("[^\\w]", "");
      if (word.length() < minLength) {
        shortestWords.clear();
        minLength = word.length();
        shortestWords.add(word);
      } else if (word.length() == minLength) {
        shortestWords.add(word);
      }
    }

    return new SearchResult(shortestWords, minLength);
  }
}
