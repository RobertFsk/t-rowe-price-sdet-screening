import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

public class WordSearcherTest {
  private static final String TEST_SENTENCE1 = "The cow jumped over the moon.";
  private static final String TEST_SENTENCE2 = "Hello, World";
  private WordSearcher wordSearcher = new WordSearcher();
  
  @Test
  public void shouldReturnTheLongestWordInSentence() {
    List<String> expectedWordList = Collections.singletonList("jumped");
    int expectedLength = 6;
    
    SearchResult result = wordSearcher.getLongestWordsInSentence(TEST_SENTENCE1);
    assertEquals(expectedLength, result.getLength());
    assertEquals(expectedWordList, result.getWords());
  }
  
  @Test(expected = IllegalArgumentException.class)
  public void shouldThrowAnExceptionWhenPassingNullArgument(){
    wordSearcher.getLongestWordsInSentence(null);
  }
  
  @Test
  public void shouldReturnMultipleLongestStringsInListWhenMultipleWordsInTheSentenceWithSameLength(){
    List<String> expectedWordList = Arrays.asList("Hello", "World");
    int expectedLength = 5;
    
    SearchResult result = wordSearcher.getLongestWordsInSentence(TEST_SENTENCE2);
    assertEquals(expectedWordList, result.getWords());
    assertEquals(expectedLength, result.getLength());
  }
  
  
  @Test
  public void shouldReturnTheShortestWordsInSentence(){
    List<String> expectedWordList = Arrays.asList("The", "cow", "the");
    int expectedLength = 3;
    
    SearchResult result = wordSearcher.getShortestWordsInSentence(TEST_SENTENCE1);
    assertEquals(expectedLength, result.getLength());
    assertEquals(expectedWordList, result.getWords());
  }
  
  @Test
  public void shouldReturnMultipleShortestStringsInListWhenMultipleWordsInTheSentenceWithSameLength(){
    List<String> expectedWordList = Arrays.asList("Hello", "World");
    int expectedLength = 5;
    
    SearchResult result = wordSearcher.getShortestWordsInSentence(TEST_SENTENCE2);
    assertEquals(expectedWordList, result.getWords());
    assertEquals(expectedLength, result.getLength());
  }
  
  @Test(expected = IllegalArgumentException.class)
  public void shouldThrowAnExceptionWhenPassingNullToGetShortestWords(){
    wordSearcher.getShortestWordsInSentence(null);
  }
}